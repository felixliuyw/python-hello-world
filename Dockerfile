FROM python:3.8-slim
WORKDIR ./py
COPY run.py ./
ENTRYPOINT [ "python" ]
CMD [ "./run.py" ]
